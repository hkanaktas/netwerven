<?php

namespace Netwerven\Vacancy\Sources\Contracts;

/**
 * Interface SourceContract
 */
interface SourceContract {

    /**
     * @return mixed
     */
    public function import();

}
