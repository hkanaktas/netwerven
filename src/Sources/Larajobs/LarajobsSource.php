<?php

namespace Netwerven\Vacancy\Sources\Larajobs;

use Netwerven\Vacancy\Sources\Contracts\SourceContract;

/**
 * Class LarajobsSource
 */
class LarajobsSource implements SourceContract {

    /**
     * Make API calls etc.
     *
     * @return array
     */
    public function import()
    {
        return [];
    }

}
