<?php

namespace Netwerven\Vacancy;

use Illuminate\Support\ServiceProvider;

class VacancyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/vacancies.php', 'vacancies');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('vacancy', function () { return new VacancyRepository(new Vacancy); });
    }
}
