<?php

namespace Netwerven\Vacancy;

use Netwerven\Vacancy\Sources\Contracts\SourceContract;
use Cache;
use Illuminate\Database\Eloquent\Model;

/**
 * Class VacancyRepository
 */
class VacancyRepository {

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $model
     */
    public function __construct(Model $model = null)
    {
        if ( ! $model) {
            $model = new Vacancy;
        }

        $this->setModel($model);
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param Model $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return array
     */
    public function all()
    {
        Cache::remember($this->getCacheKey(), $this->getCacheTimeout(), function ()
        {
            return $this->model->newQuery()->get();
        });
    }

    /**
     * @param array $data
     * @return Model
     */
    public function add($data)
    {
        return $this->model->create($data);
    }

    /**
     * Imports from all sources and persists to database
     */
    public function importFromSources()
    {
        $sources = $this->sources();

        foreach ($sources as $source)
        {
            array_map([$this, 'add'], $source->import());
        }
    }

    /**
     * @return SourceContract[]
     */
    public function sources()
    {
        $sources = [];

        foreach ($this->getSources() as $source)
        {
            $source = sprintf(__NAMESPACE__ . '\\Sources\\%1$s\\%1$sSource', $source);

            $sources[] = app($source);
        }

        return $sources;
    }

    /**
     * @return mixed
     */
    public function getSources()
    {
        return config('vacancies.sources');
    }

    /**
     * @return mixed
     */
    protected function getCacheKey()
    {
        return config('vacancies.cache-key');
    }

    /**
     * @return mixed
     */
    protected function getCacheTimeout()
    {
        return config('vacancies.cache-timeout');
    }

}
