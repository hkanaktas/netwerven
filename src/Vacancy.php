<?php

namespace Netwerven\Vacancy;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Vacancy
 */
class Vacancy extends Model {

    /**
     * @var array
     */
    protected $fillable = ['title', 'description', 'meta'];

    /**
     * @param $original
     * @return mixed
     */
    public function getMetaAttribute($original)
    {
        return (array) json_decode($original);
    }

    /**
     * @param $data
     */
    public function setMetaAttribute($data)
    {
        $this->attributes['meta'] = json_encode($data);
    }

}
