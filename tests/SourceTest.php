<?php

class SourceTest extends TestCase {

    public function tearDown()
    {
        Mockery::close();
    }

    public function testImport()
    {
        $attr = [
            'title' => 'Larajob Posting',
            'description' => 'some desc',
            'meta' => json_encode(['a' => 'b'])
        ];

        $source = Mockery::mock('Netwerven\\Vacancy\\Sources\\Larajobs\\LarajobsSource');
        $source->shouldReceive('import')->andReturn([ $attr ]);
        $this->app->instance('Netwerven\\Vacancy\\Sources\\Larajobs\\LarajobsSource', $source);

        $model = Mockery::mock('Netwerven\\Vacancy\\Vacancy');
        $model->shouldReceive('create')->withArgs($attr);

        \Config::shouldReceive('get')->with('vacancies.sources', null)->andReturn(['Larajobs']);

        $repo = new Netwerven\Vacancy\VacancyRepository($model);

        $this->assertEquals($attr, $repo->sources()[0]->import()[0]);
    }

}
 