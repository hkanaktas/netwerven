<?php

class ModelTest extends TestCase {

    public function tearDown()
    {
        Mockery::close();
    }

    public function testMetaSetter()
    {
        $meta = [
            'a' => 'b'
        ];

        $model = new \Netwerven\Vacancy\Vacancy([
            'title' => 'Test',
            'description' => 'test',
            'meta' => [
                'some-key' => 'some-value'
            ]
        ]);

        $model->meta = $meta;

        $this->assertEquals($meta, $model->meta);
    }

}
